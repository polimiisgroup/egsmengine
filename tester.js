var GSM           = require('./modules/GSMManager');
var EventManager  = require('./modules/EventManager');
var conf          = require('./config/conf_test');
const assert      = require('assert');
var LM            = require('./modules/LogManager');
var errorCount    = 0;


/***************************UTILITY METHODS**************************/

function startTest(name){
  try{
    log(name + ' - start');
    LM.log('Test started ' + name);
    if(name.indexOf('xor') > -1){
      GSM.start(conf.processModelPath_xor, conf.infoModelPath_xor);
    }
    else{
      GSM.start(conf.processModelPath_test, conf.infoModelPath_test);
    }
    //run test
    tests[name]();
    GSM.reset();
    log(name + ' - end\n\n');
  }
  catch(error)
  {
    errorCount++;
    log(error);
  }
}

function raiseEvent(name, value)
{
  //EventManager.emitter.emit(name, name, value);
  GSM.updateInfoModel(name,value);
  log('Event --> ' + name + '['+ value + ']');
  //log('-------Check stages e process guards-------');
  //checkOpenedStages();
  //checkActiveProcessGuards();
  //log('-------------------------------------------');
}

function log(message)
{
  console.log(message);
  LM.logTest(message);
}

function checkStage (id, state, compliance, status){
  var stage = GSM.getStage(id);
  var actual_state = stage.state;
  var actual_compliance = stage.compliance;
  var actual_status = stage.status;
  log('Stage: ' + id);
  log('-expect: ' + state + ' - ' + compliance + ' - ' + status);
  log('-actual: ' + actual_state + ' - ' + actual_compliance + ' - ' + actual_status);
  assert(actual_state == state);
  assert(actual_compliance == compliance);
  assert(actual_status == status);
}

function checkData (id, value){
  var data = GSM.getData(id);
  var actual_value = data.value;

  log(data.type + ' - ' + id);
  log('-expect: ' + value);
  log('-actual: ' + actual_value);
  assert(actual_value == value);
}

function checkActiveProcessGuards (){
  var dataArray = GSM.getAllData();
  for(var item in dataArray)
  {
    if(dataArray[item].type == 'P' && dataArray[item].value)
    {
      log('PFG active: ' + dataArray[item].name);
    }
  }
}

function checkOpenedStages (){
  var stageArray = GSM.getAllStage();
  for(var item in stageArray)
  {
    if(stageArray[item].state == 'opened')
    {
      log('STAGE open: ' + stageArray[item].name);
    }
  }
}

function checkStageCompliance (){
  var stageArray = GSM.getAllStage();
  for(var item in stageArray)
  {
    if(stageArray[item].compliance != 'onTime')
    {
      log('STAGE compliance error: ' + stageArray[item].name + ' - ' + stageArray[item].compliance);
      throw new Error('STAGE compliance error');
    }
  }
}


/***************************TEST CASES**************************/

tests = {};
tests.t1_start_and_end = function(){
  checkStage('Container_LC', 'opened', 'onTime', 'regular');
  //step1
  raiseEvent('StartEvent_1', true);
  checkStage('StartEvent_1', 'closed', 'onTime', 'regular');
  checkStage('casestudy', 'opened', 'onTime', 'regular');
  //step2
  raiseEvent('EndEvent_1', true);
  checkStage('EndEvent_1', 'closed', 'outOfOrder', 'regular');
  checkStage('casestudy', 'opened', 'onTime', 'regular');
}

tests.t2_double_start = function(){
  //step1
  raiseEvent('StartEvent_1', true);
  checkStage('StartEvent_1', 'closed', 'onTime', 'regular');
  //step2
  raiseEvent('StartEvent_1', true);
  checkStage('StartEvent_1', 'closed', 'outOfOrder', 'regular');
}

tests.t3_double_end = function(){
  //step1
  raiseEvent('EndEvent_1', true);
  checkStage('EndEvent_1', 'closed', 'outOfOrder', 'regular');
  checkStage('casestudy', 'opened', 'onTime', 'regular');
  //step2
  raiseEvent('EndEvent_1', true);
  checkStage('EndEvent_1', 'closed', 'outOfOrder', 'regular');
  checkStage('casestudy', 'opened', 'onTime', 'regular');
}

tests.t4_complete_execution_1 = function(){
  //step1
  raiseEvent('StartEvent_1', true);
  checkStage('StartEvent_1', 'closed', 'onTime', 'regular');
  checkStage('casestudy', 'opened', 'onTime', 'regular');
  //step2
  raiseEvent('Container', 'EmptyWarehouseUnhooked');
  checkStage('ProvideContainer', 'unopened', 'onTime', 'regular');
  //step3
  raiseEvent('Truck', 'MtoMoving');
  checkStage('ProvideContainer', 'opened', 'onTime', 'regular');
  //step4
  raiseEvent('Container', 'EmptyLoading_areaUnhooked');
  checkStage('ProvideContainer', 'closed', 'onTime', 'regular');
  checkStage('Container_EmptyWarehouseUnhooked', 'closed', 'onTime', 'regular');
  checkStage('Container_EmptyLoading_areaUnhooked', 'opened', 'onTime', 'regular');
  //step5
  raiseEvent('Truck', 'MtoStill');
  checkStage('PickUpContainer', 'opened', 'onTime', 'regular');
  //step6
  raiseEvent('Container', 'EmptyLoading_areaHooked');
  checkStage('PickUpContainer', 'closed', 'onTime', 'regular');
  checkStage('GoToProducer', 'unopened', 'onTime', 'regular');
  checkStage('Container_EmptyLoading_areaUnhooked', 'closed', 'onTime', 'regular');
  checkStage('Container_EmptyLoading_areaHooked', 'opened', 'onTime', 'regular');
  //step7
  raiseEvent('Truck', 'MtoMoving');
  checkStage('GoToProducer', 'opened', 'onTime', 'regular');
  //step8
  raiseEvent('Container', 'EmptyShippingHooked');
  checkStage('GoToProducer', 'opened', 'onTime', 'regular');
  checkStage('Container_EmptyLoading_areaHooked', 'closed', 'onTime', 'regular');
  checkStage('Container_EmptyShippingHooked', 'opened', 'onTime', 'regular');
  //step9
  raiseEvent('Truck', 'ProducerStill');
  checkStage('GoToProducer', 'closed', 'onTime', 'regular');
  checkStage('VerifyCarrierIdentity', 'opened', 'onTime', 'regular');
  //step10
  raiseEvent('Authorization', 'Approved');
  checkStage('VerifyCarrierIdentity', 'closed', 'onTime', 'regular');
  //step11
  raiseEvent('Goods', 'PackedUndamaged');
  checkStage('LoadGoods', 'opened', 'onTime', 'regular');
  checkStage('BoundaryEvent_2_exception', 'opened', 'onTime', 'regular');
  checkStage('BoundaryEvent_2_flow2', 'opened', 'onTime', 'regular');
  //step12
  raiseEvent('Container', 'FullShippingHooked');
  checkStage('LoadGoods', 'closed', 'onTime', 'regular');
  checkStage('Container_EmptyShippingHooked', 'closed', 'onTime', 'regular');
  checkStage('Container_FullShippingHooked', 'opened', 'onTime', 'regular');
  //step13
  raiseEvent('ShipmentDocs', 'Ready');
  checkStage('CheckDoc', 'opened', 'onTime', 'regular');
  checkStage('SequenceFlow_7', 'opened', 'onTime', 'regular');
  checkStage('ExclusiveGateway_4_iteration', 'opened', 'onTime', 'regular');
  checkStage('ExclusiveGateway_4', 'opened', 'onTime', 'regular');
  //step14
  raiseEvent('ShipmentDocs', 'Complete');
  checkStage('CheckDoc', 'closed', 'onTime', 'regular');
  checkStage('BoundaryEvent_2_exception', 'closed', 'onTime', 'regular');
  checkStage('BoundaryEvent_2_flow2', 'closed', 'onTime', 'regular');
  checkStage('SequenceFlow_7', 'closed', 'onTime', 'regular');
  checkStage('ExclusiveGateway_4_iteration', 'closed', 'onTime', 'regular');
  checkStage('ExclusiveGateway_4', 'closed', 'onTime', 'regular');
  //step15
  raiseEvent('Truck', 'ProducerMoving');
  checkStage('StartShipment', 'opened', 'onTime', 'regular');
  //step16
  raiseEvent('Truck', 'HighwayMoving');
  checkStage('StartShipment', 'closed', 'onTime', 'regular');
  //stepXX
  raiseEvent('EndEvent_1',  true);
  checkStageCompliance();
  checkStage('EndEvent_1', 'closed', 'onTime', 'regular');
  checkStage('casestudy', 'closed', 'onTime', 'regular');
}

tests.t5_container_lifeCycle_skipped_outOfOrder = function(){
  checkStage('casestudy', 'unopened', 'onTime', 'regular');
  //step1
  raiseEvent('Container', 'EmptyLoading_areaUnhooked');
  checkStage('ProvideContainer', 'unopened', 'onTime', 'regular');
  checkStage('Container_EmptyWarehouseUnhooked', 'unopened', 'skipped', 'regular');
  checkStage('Container_EmptyLoading_areaUnhooked', 'opened', 'outOfOrder', 'regular');
}

tests.t6_container_lifeCycle_onTime_skipped_outOfOrder = function(){
  checkStage('casestudy', 'unopened', 'onTime', 'regular');
  checkStage('Container_LC', 'opened', 'onTime', 'regular');
  //step1
  raiseEvent('Container', 'FullShippingHooked');
  checkStage('casestudy', 'unopened', 'onTime', 'regular');
  checkStage('ProvideContainer', 'unopened', 'onTime', 'regular');
  checkStage('Container_LC', 'opened', 'onTime', 'regular');
  //check behavior with E-GSM specifications
  checkStage('Container_EmptyWarehouseUnhooked', 'unopened', 'onTime', 'regular');
  checkStage('Container_EmptyLoading_areaUnhooked', 'unopened', 'onTime', 'regular');
  checkStage('Container_EmptyLoading_areaHooked', 'unopened', 'onTime', 'regular');
  checkStage('Container_EmptyShippingHooked', 'unopened', 'skipped', 'regular');
  checkStage('Container_FullShippingHooked', 'opened', 'outOfOrder', 'regular');
  checkStage('Container_final', 'opened', 'onTime', 'regular');
  checkStage('Container_error', 'unopened', 'onTime', 'regular');
}

tests.t7_reopen_stage = function(){
  //step1
  raiseEvent('StartEvent_1', true);
  checkStage('StartEvent_1', 'closed', 'onTime', 'regular');
  checkStage('casestudy', 'opened', 'onTime', 'regular');
  //step2
  raiseEvent('Container', 'EmptyWarehouseUnhooked');
  checkStage('ProvideContainer', 'unopened', 'onTime', 'regular');
  //step3
  raiseEvent('Truck', 'MtoMoving');
  checkStage('ProvideContainer', 'opened', 'onTime', 'regular');
  //step4
  raiseEvent('Container', 'EmptyLoading_areaUnhooked');
  checkStage('ProvideContainer', 'closed', 'onTime', 'regular');
  checkStage('Container_EmptyWarehouseUnhooked', 'closed', 'onTime', 'regular');
  checkStage('Container_EmptyLoading_areaUnhooked', 'opened', 'onTime', 'regular');
  //step5
  raiseEvent('Truck', 'MtoStill');
  checkStage('PickUpContainer', 'opened', 'onTime', 'regular');
  //step6
  raiseEvent('Container', 'EmptyLoading_areaHooked');
  checkStage('PickUpContainer', 'closed', 'onTime', 'regular');
  checkStage('GoToProducer', 'unopened', 'onTime', 'regular');
  checkStage('Container_EmptyLoading_areaUnhooked', 'closed', 'onTime', 'regular');
  checkStage('Container_EmptyLoading_areaHooked', 'opened', 'onTime', 'regular');
  //step7
  raiseEvent('Container', 'EmptyLoading_areaUnhooked');
  checkStage('PickUpContainer', 'opened', 'outOfOrder', 'regular');
  checkStage('ProvideContainer', 'closed', 'onTime', 'regular');
  checkStage('Container_EmptyWarehouseUnhooked', 'closed', 'onTime', 'regular');
  checkStage('Container_EmptyLoading_areaUnhooked', 'opened', 'outOfOrder', 'regular');
}

tests.t8_complete_execution_1_restart = function(){
  //step1
  raiseEvent('StartEvent_1', true);
  //step2
  raiseEvent('Container', 'EmptyWarehouseUnhooked');
  //step3
  raiseEvent('Truck', 'MtoMoving');
  //step4
  raiseEvent('Container', 'EmptyLoading_areaUnhooked');
  //step5
  raiseEvent('Truck', 'MtoStill');
  //step6
  raiseEvent('Container', 'EmptyLoading_areaHooked');
  //step7
  raiseEvent('Truck', 'MtoMoving');
  //step8
  raiseEvent('Container', 'EmptyShippingHooked');
  //step9
  raiseEvent('Truck', 'ProducerStill');
  //step10
  raiseEvent('Authorization', 'Approved');
  //step11
  raiseEvent('Goods', 'PackedUndamaged');
  //step12
  raiseEvent('Container', 'FullShippingHooked');
  //step13
  raiseEvent('ShipmentDocs', 'Ready');
  //step14
  raiseEvent('ShipmentDocs', 'Complete');
  //step15
  raiseEvent('Truck', 'ProducerMoving');
  //step16
  raiseEvent('Truck', 'HighwayMoving');
  //step17
  raiseEvent('EndEvent_1',  true);
  checkStageCompliance();
  //step18
  raiseEvent('StartEvent_1', true);
  checkStage('ProvideContainer', 'unopened', 'onTime', 'regular');
  checkStage('EndEvent_1', 'unopened', 'onTime', 'regular');
  checkStage('StartShipment', 'unopened', 'onTime', 'regular');
  checkStage('VerifyCarrierIdentity', 'unopened', 'onTime', 'regular');
  checkStage('PickUpContainer', 'unopened', 'onTime', 'regular');
  checkStage('GoToProducer', 'unopened', 'onTime', 'regular');
  checkStage('BoundaryEvent_2_exception', 'unopened', 'onTime', 'regular');
  checkStage('BoundaryEvent_2_flow1', 'unopened', 'onTime', 'regular');
  checkStage('IntermediateThrowEvent_1', 'unopened', 'onTime', 'regular');
  checkStage('BoundaryEvent_2_flow2', 'unopened', 'onTime', 'regular');
  checkStage('ExclusiveGateway_4', 'unopened', 'onTime', 'regular');
  checkStage('ExclusiveGateway_4_iteration', 'unopened', 'onTime', 'regular');
  checkStage('SequenceFlow_7', 'unopened', 'onTime', 'regular');
  checkStage('CheckDoc', 'unopened', 'onTime', 'regular');
  checkStage('SequenceFlow_9', 'unopened', 'onTime', 'regular');
  checkStage('UpdateDoc', 'unopened', 'onTime', 'regular');
  checkStage('LoadGoods', 'unopened', 'onTime', 'regular');
  checkStageCompliance();
}

tests.t9_child_and_parent_closed = function(){
  //step1
  raiseEvent('StartEvent_1', true);
  //step2
  raiseEvent('Container', 'EmptyWarehouseUnhooked');
  //step3
  raiseEvent('Truck', 'MtoMoving');
  //step4
  raiseEvent('Container', 'EmptyLoading_areaUnhooked');
  //step5
  raiseEvent('Truck', 'MtoStill');
  //step6
  raiseEvent('Container', 'EmptyLoading_areaHooked');
  //step7
  raiseEvent('Truck', 'MtoMoving');
  //step8
  raiseEvent('Container', 'EmptyShippingHooked');
  //step9
  raiseEvent('Truck', 'ProducerStill');
  //step10
  raiseEvent('BoundaryEvent_2', true);
  checkStage('VerifyCarrierIdentity', 'closed', 'onTime', 'faulty');
  //step11
  raiseEvent('IntermediateThrowEvent_1', true);
  checkStage('BoundaryEvent_2_flow1', 'closed', 'onTime', 'regular');
  checkStage('IntermediateThrowEvent_1', 'closed', 'onTime', 'regular');
  checkStageCompliance();
}

tests.t10_all_stage_closed = function(){
  //step1
  raiseEvent('StartEvent_1', true);
  //step2
  raiseEvent('Container', 'EmptyWarehouseUnhooked');
  //step3
  raiseEvent('Truck', 'MtoMoving');
  //step4
  raiseEvent('Container', 'EmptyLoading_areaUnhooked');
  //step5
  raiseEvent('Truck', 'MtoStill');
  //step6
  raiseEvent('Container', 'EmptyLoading_areaHooked');
  //step7
  raiseEvent('Truck', 'MtoMoving');
  //step8
  raiseEvent('Container', 'EmptyShippingHooked');
  //step9
  raiseEvent('Truck', 'ProducerStill');
  //step10
  raiseEvent('Authorization', 'Approved');
  //step11
  raiseEvent('Goods', 'PackedUndamaged');
  //step12
  raiseEvent('Container', 'FullShippingHooked');
  //step13
  raiseEvent('ShipmentDocs', 'Ready');
  //step14
  raiseEvent('ShipmentDocs', 'Incomplete');
  //step15
  raiseEvent('ShipmentDocs', 'Ready');
  //step16
  raiseEvent('ShipmentDocs', 'Complete');
  //step17
  raiseEvent('Truck', 'ProducerMoving');
  //step18
  raiseEvent('Truck', 'HighwayMoving');
  //step19
  raiseEvent('EndEvent_1', true);
  //ensure that all stages except lifecycle are closed
  checkStage('ProvideContainer', 'closed', 'onTime', 'regular');
  checkStage('EndEvent_1', 'closed', 'onTime', 'regular');
  checkStage('StartShipment', 'closed', 'onTime', 'regular');
  checkStage('VerifyCarrierIdentity', 'closed', 'onTime', 'regular');
  checkStage('PickUpContainer', 'closed', 'onTime', 'regular');
  checkStage('GoToProducer', 'closed', 'onTime', 'regular');
  checkStage('BoundaryEvent_2_exception', 'closed', 'onTime', 'regular');
  checkStage('BoundaryEvent_2_flow1', 'unopened', 'onTime', 'regular');
  checkStage('IntermediateThrowEvent_1', 'unopened', 'onTime', 'regular');
  checkStage('BoundaryEvent_2_flow2', 'closed', 'onTime', 'regular');
  checkStage('ExclusiveGateway_4', 'closed', 'onTime', 'regular');
  checkStage('ExclusiveGateway_4_iteration', 'closed', 'onTime', 'regular');
  checkStage('SequenceFlow_7', 'closed', 'outOfOrder', 'regular');
  checkStage('CheckDoc', 'closed', 'onTime', 'regular');
  checkStage('SequenceFlow_9', 'closed', 'onTime', 'regular');
  checkStage('UpdateDoc', 'closed', 'onTime', 'regular');
  checkStage('LoadGoods', 'closed', 'onTime', 'regular');
}

/***************************XOR TEST CASES**************************/

tests.t1_xor_process_guard_info_model = function(){
  //step1
  raiseEvent('DayOfWeek', 'Sunday');
  //step2
  raiseEvent('Start_shipping', true);
  //step3
  raiseEvent('Truck', 'Producer');
  //step4
  raiseEvent('Container', 'Full');
  //step5
  raiseEvent('Truck', 'SiteA');
  //step6
  raiseEvent('Truck', 'HighwayToB');
  //all stages must be onTime
  checkStageCompliance();
}


/***************************START TEST ROUTINES****************************/

log('Start testing...\n\n');
var array_tests = Object.keys(tests);
for(var it in array_tests)
{
  //start test case
  startTest(array_tests[it]);
}

//before terminating execution, check if any error occurred
if(errorCount > 0)
{
  log('\n\nWARNING! There are errors in tests. Error count: '+  errorCount);
}
else {
  log('\n\nSUCCESS! All PASSED with no errors!');
}
