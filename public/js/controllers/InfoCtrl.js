angular.module('InfoCtrl', []).controller('InfoController', function($scope, $http, $window, $interval) {
  $scope.loadInfoModel = function()
  {
    $http.get('api/infoModel').
    success(function(data, status, headers, config) {
      $scope.infos = data;
    }).
    error(function(data, status, headers, config) {
      // log error
      $window.alert('error loadInfoModel: ' + data);
    });
  }

  $scope.updateInfoModel = function(name, value)
  {
    console.log(name,value);
    $http.get('api/updateInfoModel?name='+name+'&value='+value).
    success(function(data, status, headers, config) {
      // $window.alert('ok');
    }).
    error(function(data, status, headers, config) {
      // log error
      $window.alert('error updateInfoModel: ' + data);
    });
    //$window.alert(event + '-' + value);
  }
});
