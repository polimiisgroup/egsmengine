angular.module('MainCtrl', []).controller('MainController', function($scope, $http, $window, $interval) {

	$scope.tagline = 1;//'To the moon and back!';



	$http.get('api/processModelPath').
	success(function(data, status, headers, config) {
		$scope.processModelPath = data;
	}).
	error(function(data, status, headers, config) {
		// log error
		$scope.processModelPath = data;
	});

	$http.get('api/infoModelPath').
	success(function(data, status, headers, config) {
		$scope.infoModelPath = data;
	}).
	error(function(data, status, headers, config) {
		// log error
		$scope.infoModelPath = data;
	});

	$scope.start = function()
	{
		$http.get('api/start?processModelPath=' + $scope.processModelPath + '&infoModelPath=' + $scope.infoModelPath).
		success(function(data, status, headers, config) {

		}).
		error(function(data, status, headers, config) {
			// log error
			$window.alert('error start: ' + data);
		});
	}

	$scope.reset = function()
	{
		$http.get('api/reset').
		success(function(data, status, headers, config) {

		}).
		error(function(data, status, headers, config) {
			// log error
			$window.alert('error reset');
		});
	}

	$scope.initModel2 = function()
	{
		$http.get('api/initModel2').
		success(function(data, status, headers, config) {
			// $window.alert('ok');
			$http.get('api/externals').
			success(function(data, status, headers, config) {
				$scope.externals = data;
			}).
			error(function(data, status, headers, config) {
				// log error
			});
		}).
		error(function(data, status, headers, config) {
			// log error
			$window.alert('error');
		});
		//$window.alert(event + '-' + value);

	}

	$interval(function() {
		$scope.tagline++;
		$http.get('api/guards').
		success(function(data, status, headers, config) {
			$scope.guards = data;
		}).
		error(function(data, status, headers, config) {
			// log error
		});

		$http.get('api/stages').
		success(function(data, status, headers, config) {
			$scope.stages = data;
		}).
		error(function(data, status, headers, config) {
			// log error
		});

		$http.get('api/environments').
		success(function(data, status, headers, config) {
			$scope.environments = data;
		}).
		error(function(data, status, headers, config) {
			// log error
		});

		$http.get('api/config_stages').
		success(function(data, status, headers, config) {
			$scope.config_stages = data;
		}).
		error(function(data, status, headers, config) {
			// log error
		});


	}, 1000);
});
