angular.module('appRoutes', []).config(
	['$routeProvider',
	'$locationProvider',
	function($routeProvider, $locationProvider) {

	$routeProvider
	
		.when('/', {
			templateUrl: 'views/home.html',
			controller: 'MainController'
		})

		.when('/info', {
			templateUrl: 'views/info.html',
			controller: 'InfoController'
		})

		.when('/lifeCycle', {
			templateUrl: 'views/lifeCycle.html',
			controller: 'LifeCycleController'
		})

		.when('/process', {
			templateUrl: 'views/process.html',
			controller: 'ProcessController'
		})

		.when('/log', {
			templateUrl: 'views/log.html',
			controller: 'LogController'
		})

	$locationProvider.html5Mode(true);

}]);
