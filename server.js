// dependencies
var express        = require('express');
var app            = express();
var bodyParser     = require('body-parser');
var methodOverride = require('method-override');
var morgan         = require('morgan');
var GSMManager     = require('./modules/GSMManager');
var conf          = require('./config/conf');
//var Sensor         = require('./modules/SensorManager');

if(process.argv.length > 4) {
	var inputPort = process.argv[2];
	var egsmModel = process.argv[3];
	var infoModel = process.argv[4];
} else if(process.argv.length > 3){
    var inputPort = process.argv[2];
    var restorepath = process.argv[3];
}

// var inputPort = process.argv[2];
// configure web server
var port;
if(inputPort > 8000 && inputPort < 65536){
  port = process.env.PORT || inputPort;
}
else{
  port = process.env.PORT || conf.port;
}

if(egsmModel!=null && infoModel!=null) {
    GSMManager.start(egsmModel,infoModel);
} else if (restorepath!=null){
    GSMManager.loadStaticState(restorepath);
}

app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(express.static(__dirname + '/public'));
require('./app/routes')(app);

// start apllication
app.listen(port);
console.log('Application started, services accessible on port ' + port);
exports = module.exports = app;

