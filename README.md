# PASO - E-GSM Process Monitoring Engine

For furthere information read: https://doi.org/10.1016/j.is.2017.12.009

# Running PASO through Docker

* Make sure Docker and Docker compose are installed and properly configured on your host system

* (optional) Copy E-GSM process model and information model files into project root folder (you can use BPMN2EGSM to generate them from a BPMN process model)

* from the root directory of the repository, execute the following commands
```
#!batch

docker-compose create

docker-compose start
```

* With a web browser, reach PASO console at http://localhost:8083

* Load the process model and information model files specifying their path (relative to project root folder) in the homonimous text fields, then click the Load button

# Running PASO locally

## Prerequisites

* Node.js 4.4
* (Optional) BPMN2EGSM translator: https://bitbucket.org/polimiisgroup/bpmn2egsm

## Configuration

* Copy E-GSM process model and information model files into project root folder (you can use BPMN2EGSM to generate them from a BPMN process model)

* Within a console terminal/command prompt, navigate to project root folder, then execute the following commands
```
#!batch

npm install

node server.js
```

* With a web browser, reach PASO console at http://localhost:8083

* Load the process model and information model files specifying their path (relative to project root folder) in the homonimous text fields, then click the Load button