var Client = require('node-rest-client').Client;
var client = new Client();

var conf                  = require('../config/conf');

var nonce = 0

var resetNonce = function() {
    nonce = 0;
}

var notifyStageOpen = function(stage, compliance)
{
    var registered = false;
    if (conf.feedbackEndpointOpen != undefined && conf.feedbackEndpointOpen != "") {
        client.registerMethod("notifyStageOpen", conf.feedbackEndpointOpen, "GET");
        registered = true;
    }
    //for compatibility with VIOLA
    else if (conf.feedbackEndpoint != undefined && conf.feedbackEndpoint != "") {
        client.registerMethod("notifyStageOpen", conf.feedbackEndpoint, "GET");
        registered = true;
    }
    
    if(registered) {
        var args = {};
        args['parameters'] = {"stageName": stage, "compliance": compliance, "timestamp": nonce++};
        var req = client.methods.notifyStageOpen(args, function (data, response) {
        });
        req.on('error', function (err) {
			console.log('something went wrong contacting the feedback endpoint!!', err.request.options);
        });
    }
}

var notifyStageClosed = function(stage, compliance)
{
    var registered = false;
    if (conf.feedbackEndpointClose != undefined && conf.feedbackEndpointClose != "") {
        client.registerMethod("notifyStageClosed", conf.feedbackEndpointClose, "GET");
        registered = true;
    }
    
    if(registered) {
        var args = {};
        args['parameters'] = {"stageName": stage, "compliance": compliance, "timestamp": nonce++};
        var req = client.methods.notifyStageClosed(args, function (data, response) {
        });
        req.on('error', function (err) {
			console.log('something went wrong contacting the feedback endpoint!!', err.request.options);
        });
    }
}

// exposed functions
module.exports = {
    notifyStageOpen: notifyStageOpen,
    notifyStageClosed: notifyStageClosed,
    resetNonce: resetNonce
}

