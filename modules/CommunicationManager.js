// DDS communication module
//var rti   = require('rticonnextdds-connector');
// "rticonnextdds-connector": "latest",
var EventManager          = require('./EventManager');
var fs                    = require('fs');
var xml2js                = require('xml2js');

var connector;
var output;
var input;

function init(info_array)
{
  // load process model and information model
  var xmlDDS = fs.readFileSync("./config/PasoDDS.xml", 'utf8');

  // parse XML and XSD files and start E-GSM engine
  var parseString = xml2js.parseString;
  parseString(xmlDDS, function (err, result) {
    //TODO, add error handling
    var DDSConf = result;
    //console.log('Communication Manager -> configured!');

    var domain = DDSConf['dds']['domain_library'][0]['domain'][0];
    var partecipant = DDSConf['dds']['participant_library'][0]['domain_participant'][0];
    // var publisher_template = partecipant['publisher'];
    // var subscriber_template = partecipant['subscriber'];
    domain['$'].domain_id='test';
    for(var key in info_array)
    {
      var topic_template = {};
      topic_template['$']={};
      topic_template['$'].name = info_array[key].name;
      topic_template['$'].register_type_ref = 'PasoType';
      if(domain['topic'] == undefined)
      {
        domain['topic'] = [];
        domain['topic'].push(topic_template);
      }
      else
      {
        domain['topic'].push(topic_template);
      }

      if(info_array[key].pub == "true")
      {
        var publisher_template = {};
        publisher_template['$']={};
        publisher_template['$'].name = 'prova';
        publisher_template['data_writer'] = {};
        publisher_template['data_writer']['$'] = {};
        publisher_template['data_writer']['$'].name = 'writer_name';
        publisher_template['data_writer']['$'].topic_ref  = info_array[key].name;
        if(partecipant['publisher'] == undefined)
        {
          partecipant['publisher'] = [];
          partecipant['publisher'].push(publisher_template);
        }
        else
        {
          partecipant['publisher'].push(publisher_template);
        }
      }
      else if(info_array[key].sub == "true")
      {
        var subscriber_template = {};
        subscriber_template['$']={};
        subscriber_template['$'].name = 'prova_reader';
        subscriber_template['data_reader'] = {};
        subscriber_template['data_reader']['$'] = {};
        subscriber_template['data_reader']['$'].name = 'reader_name';
        subscriber_template['data_reader']['$'].topic_ref  = info_array[key].name;
        if(partecipant['subscriber'] == undefined)
        {
          partecipant['subscriber'] = [];
          partecipant['subscriber'].push(subscriber_template);
        }
        else
        {
          partecipant['subscriber'].push(subscriber_template);
        }
      }
      // console.log(info_array[key].name);
      // console.log(info_array[key].pub);
      // console.log(info_array[key].sub);
    }

    var builder = new xml2js.Builder();
    var xml = builder.buildObject(DDSConf);
    fs.writeFile("./config/PasoDDS_new.xml", xml, function (err, obj) { console.log(err);});
  });



  // connector = new rti.Connector("PasoParticipantLibrary::Zero",__dirname + "/../PasoDDS.xml");
  // output = connector.getOutput("MyPublisher::PasoWriter");
  // input = connector.getInput("MySubscriber::PasoReader");

  function write()
  {
    output.instance.setString("name", "container_1111");
    output.instance.setString("status", "{ prova = ciao }");
    console.log("Writing...");
    output.write();
  }
}

module.exports = {
  init: init
}
