// sensor handling module
// "raspi-io": "latest",
// dependencies
var five = require("johnny-five");
var Galileo = require("galileo-io");
var EventManager = require('./EventManager');

// initialize Galileo board
var board = new five.Board({
  io: new Galileo()
});

// handle 'ready' event
board.on("ready", function() {

  // var temperature = new five.Thermometer({
  //   controller: "LM35",
  //   pin: "A0"
  //       });
  //
  // temperature.on("data", function() {
  //   console.log(this.celsius + "°C", this.fahrenheit + "°F");
  //   EventManager.emitter.emit('A', 'A', this.celsius);
  // });

  var accelerometer = new five.Accelerometer({
    controller: "MMA7361",
    pins: ["A0", "A1", "A2"],
    sleepPin: 12,
    autoCalibrate: true
  });

  accelerometer.on("change", function() {
    console.log("accelerometer");
    console.log("  x            : ", this.x);
    console.log("  y            : ", this.y);
    console.log("  z            : ", this.z);
    console.log("  pitch        : ", this.pitch);
    console.log("  roll         : ", this.roll);
    console.log("  acceleration : ", this.acceleration);
    console.log("  inclination  : ", this.inclination);
    console.log("  orientation  : ", this.orientation);
    console.log("--------------------------------------");
  });

  // Create a new `motion` hardware instance.
  var motion = new five.Motion(7);

  // "calibrated" occurs once, at the beginning of a session,
  motion.on("calibrated", function() {
    console.log("calibrated");
  });

  // "motionstart" events are fired when the "calibrated"
  // proximal area is disrupted, generally by some form of movement
  motion.on("motionstart", function() {
    console.log("motionstart");
  });

  // "motionend" events are fired following a "motionstart" event
  // when no movement has occurred in X ms
  motion.on("motionend", function() {
    console.log("motionend");
  });

  // "data" events are fired at the interval set in opts.freq
  // or every 25ms. Uncomment the following to see all
  // motion detection readings.
  // motion.on("data", function(data) {
  //   console.log(data);
  // });
});
