var GSMManager = require('../modules/GSMManager');
var EventManager = require('../modules/EventManager');
var LogManager = require('../modules/LogManager');
var conf = require('../config/conf');
const multer = require('multer');
const path = require('path');
const fs = require('fs');

// Set up storage for multer 
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

module.exports = function (app) {

	app.get('/api/processModelPath', function (req, res) {
		res.json(conf.processModelPath);
	});

	app.get('/api/infoModelPath', function (req, res) {
		res.json(conf.infoModelPath);
	});

	app.get('/api/start', function (req, res) {
		GSMManager.start(req.query['processModelPath'], req.query['infoModelPath']);
		res.send();
	});

	app.post('/api/uploadModel', upload.fields([{ name: 'egsm', maxCount: 1 }, { name: 'infoModel', maxCount: 1 }]), function (req, res) {

		const files = req.files;

		if (!files || !files.egsm || !files.infoModel) {
			return res.status(400).send('Please upload both files.');
		}

		const timestamp = Date.now();

		const egsmPath = `data/${timestamp}-egsm.xml`;
		const infoModelPath = `data/${timestamp}-infoModel.xsd`;

		fs.writeFileSync(egsmPath, files.egsm[0].buffer);
		fs.writeFileSync(infoModelPath, files.infoModel[0].buffer);


		GSMManager.start(egsmPath, infoModelPath);
		res.send({'egsmPath': egsmPath, 'infoModelPath': infoModelPath});
	});

	app.get('/api/reset', function (req, res) {
		GSMManager.reset();
		res.end();
	});

	app.get('/api/config_stages', function (req, res) {
		res.json(GSMManager.getCompleteDiagram());
	});

	app.get('/api/config_stages_diagram', function (req, res) {
		res.json(GSMManager.getCompleteNodeDiagram());
	});

	app.get('/api/debugLog', function (req, res) {
		res.json(LogManager.debugLog);
	});

	app.get('/api/infoModel', function (req, res) {
		res.json(GSMManager.getInfoModel());
	});

	app.get('/api/updateInfoModel', function (req, res) {
		res.json(GSMManager.updateInfoModel(req.query['name'], req.query['value']));
	});

	app.post('/api/updateInfoModel', function (req, res) {
		res.json(GSMManager.updateInfoModel2(req.query['name'], Date.now(), req.body));
	});

	app.get('/api/guards', function (req, res) {
		res.json(GSMManager.Data_);
	});

	app.get('/api/stages', function (req, res) {
		res.json(GSMManager.Stage_);
	});

	app.get('/api/environments', function (req, res) {
		res.json(GSMManager.Environment_);
	});

	app.get('/api/externals', function (req, res) {
		res.json(GSMManager.getExternalModel());
	});


	app.get('*', function (req, res) {
		res.sendFile('index.html', { root: path.join(__dirname, '../public') });
	});

};
